# my-test-catalog-cicd

This is a fast test on the new feature of GitLab, the CICD Catalog.


## 👀 Links 

- GitLab documentation : https://docs.gitlab.com/ee/ci/components/ 
- GitLab Catalog : https://gitlab.com/gitlab-components 