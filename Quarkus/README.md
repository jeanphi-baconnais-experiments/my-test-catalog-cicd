# Quarkus CI/CD

## Usage

Use this component to allow Quarkus to be used in your project.
You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: gitlab.com/jeanphi-baconnais-experiments/quarkus@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.


### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| maven-version | ❌ | Maven version to use |
| jdk-version | ❌ | JDK version to use |

### Variables

| Variable | Description |
| -------- | ----------- |

## Contribute

To contribute improvements to CI/CD templates, follow the Development guide at:
https://docs.gitlab.com/ee/development/cicd/templates.html


